<?php

namespace app\controllers;

use Yii;
use app\controllers\base\AppController;
use app\models\politics\Organization;
use app\models\politics\OrganizationMembership as Membership;
use app\models\government\ConstituentAssemblyMembership as AssemblyMembership;
use app\models\politics\OrganizationPost as Post;
use app\models\variables\Ideology;
use yii\filters\VerbFilter;
use app\exceptions\NotAllowedHttpException;

/**
 * Description of OrganizationController
 *
 * @author ilya
 */
class OrganizationController extends AppController
{
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'request' => ['POST'],
                    'fire-membership' => ['POST'],
                    'set-successor-save' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'approved' => Membership::findByUserId($this->user->id)->andWhere(['is not', 'dateApproved', null])->with('org')->all(),
            'requested' => Membership::findByUserId($this->user->id)->andWhere(['dateApproved' => null])->with('org')->all(),
            'assemblies' => AssemblyMembership::findByUserId($this->user->id)->with('assembly')->all(),
            'user' => $this->user,
        ]);
    }
    
    public function actionProfile(int $id)
    {
        return $this->render('profile', [
            'model' => $this->getModel($id),
            'user' => $this->user,
        ]);
    }
    
    public function actionCreate()
    {
        $model = new Organization([]);
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $transaction = $model->getDb()->beginTransaction();
            if ($model->save() && $model->saveFlag()) {
                $membership = new Membership([
                    'orgId' => $model->id,
                    'userId' => $this->user->id,
                ]);
                if (!$membership->approve()) {
                    var_dump($membership->getErrors()); die();
                }
                $post = new Post(Post::DEFAULT_LEADER_DATA);
                $post->orgId = $model->id;
                $post->userId = $this->user->id;
                if (!$post->save()) {
                    var_dump($post->getErrors()); die();
                }
                $transaction->commit();
                return $this->redirect(['organization/profile', 'id' => $model->id]);
            } else {
                $transaction->rollBack();
            }
        }
        
        return $this->render('create', [
            'model' => $model,
            'ideologies' => Ideology::find()->all(),
        ]);
    }
    
    function actionRequest(int $id)
    {
        $model = $this->getModel($id);
        if ($model->joiningRules == Organization::JOINING_RULES_PRIVATE) {
            throw new NotAllowedHttpException();
        }
        $membership = new Membership([
            'orgId' => $id,
            'userId' => $this->user->id,
        ]);
        if ($model->joiningRules == Organization::JOINING_RULES_OPEN) {
            $membership->approve();
        } else {
            $membership->save();
        }
        $model->updateMembersCount();
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    function actionFireMembership(int $id)
    {
        $membership = Membership::findOne(['orgId' => $id, 'userId' => $this->user->id]);
        if ($membership) {
            $membership->fireSelf();
        }
        $model = $this->getModel($id);
        $model->updateMembersCount();
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    function actionSetSuccessor(int $postId=0)
    {
        if (Yii::$app->request->isPost) {
            $postId = (int)Yii::$app->request->post('postId');
            $userId = (int)Yii::$app->request->post('userId');

            $post = Post::findOne($postId);
            if (is_null($post)) {
                throw new NotFoundHttpException();
            }
            if ($post->userId != $this->user->id || $post->appointmentType != Post::APPOINTMENT_TYPE_INHERITANCE) {
                throw new NotAllowedHttpException();
            }
            $post->successorId = $userId;
            $post->save();
            return $this->redirect(Yii::$app->request->referrer);
        }
        
        $post = Post::findOne($postId);
        if (is_null($post)) {
            throw new NotFoundHttpException();
        }
        return $this->renderAjax('_set_successor', [
            'model' => $post,
            'user' => $this->user,
            'candidats' => $post->organization->getUsers()->where(['<>', 'id', $this->user->id])->all(),
        ]);
    }
    
    /**
     * 
     * @param integer $id
     * @return Organization
     * @throws NotFoundHttpException
     */
    private function getModel(int $id): Organization
    {
        $model = Organization::findOne($id);
        if (is_null($model)) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
    
}
