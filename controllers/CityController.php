<?php

namespace app\controllers;

use app\controllers\base\AppController;
use app\models\map\City;
use yii\web\NotFoundHttpException;

/**
 * 
 */
final class CityController extends AppController
{
    
    public function actionIndex($id)
    {
        $city = City::findOne($id);
        if (is_null($city)) {
            throw new NotFoundHttpException('City not found');
        }
        return $this->render('view', [
            'city' => $city,
            'user' => $this->user
        ]);
    }
    
}
