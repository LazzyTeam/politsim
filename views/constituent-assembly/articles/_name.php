<?php

use yii\widgets\ActiveForm;
use app\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\government\ConstituentAssembly */
/* @var $user app\models\auth\User */

$dataModel = (isset($model->dataObjects[$user->id])) ? $model->dataObjects[$user->id] : new \app\models\government\ConstituentAssemblyData();

?>

<?php $form = ActiveForm::begin([
    'action' => "/constituent-assembly/add-variant?assemblyId={$model->id}&article=name",
    'method' => 'POST',
]) ?>

<?= $form->field($dataModel, "name[full]")->textInput() ?>
<?= $form->field($dataModel, "name[short]")->textInput() ?>

<?= Html::submitButton('Отправить', ['class' => 'btn btn-primary btn-submit']) ?>

<?php ActiveForm::end() ?>
