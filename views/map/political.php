<?php

/* @var $this \yii\web\View */
/* @var $states \app\models\government\State[] */
/* @var $cities \app\models\map\City[] */

$this->title = Yii::t('app', 'Wolrd political map');
$this->registerJsFile("/js/map.js")

?>
<div id="map" style="width:100%; min-height: 500px; height: auto"></div>
<?php

    $js = <<<EOJS
            
    $('#map').css('min-height', $('#bigcontainer').height());
    
    var options = {center: [45, 34], zoom: 5};
    if (localStorage.getItem('zoom')) {
        options.zoom = localStorage.getItem('zoom');
    }
    if (localStorage.getItem('center-lat') && localStorage.getItem('center-lng')) {
        options.center = [localStorage.getItem('center-lat'),localStorage.getItem('center-lng')];
    }
    map = new L.map('map', options);
    
    var Stamen_TerrainBackground = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/terrain-background/{z}/{x}/{y}.{ext}', {
	attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
	subdomains: 'abcd',
	noWrap: true,
        minZoom: 2,
	maxZoom: 18,
	ext: 'png'
    });
    var Stamen_Watercolor = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.{ext}', {
        attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        subdomains: 'abcd',
        minZoom: 1,
        maxZoom: 16,
        ext: 'jpg'
    });
    // Stamen_TerrainBackground.addTo(map);
    Stamen_Watercolor.addTo(map);
    
    map.on('moveend', function(){
        localStorage.setItem('zoom',map.getZoom());
        localStorage.setItem('center-lat',map.getCenter().lat);
        localStorage.setItem('center-lng',map.getCenter().lng);
    });
    
    states = {};
    cities = {};
    
EOJS;
    
    foreach ($states as $state) {
        if (!$state->polygon) {
            continue;
        }
        $js .= "states[{$state->id}] = L.polygon([".json_encode($state->polygon->data)."],{
            color: '#000',
            opacity: 1,
            fillColor: '#{$state->mapColor}',
            fillOpacity: 0.5,
            weight: 1,
            title: '{$state->name}'
        })
        .bindTooltip('{$state->tooltipName}', {permanent: true, className: 'map-state-tooltip', offset: [0, 0], direction: 'center' })
        ;
        states[{$state->id}].addTo(map);
        
//        var bounds = states[{$state->id}].getBounds();
//        var point1 = bounds.getCenter();
//        point1.lng = bounds.getWest();
//        var point2 = bounds.getCenter();
//        point2.lng = bounds.getEast();
//        var line = L.polyline([point1, point2], {opacity:0})
//        .setText('{$state->name}', {
//                            repeat: false,
//                            center: true,
//                            attributes: {fill: '#000',
//                                         'font-weight': 'bold',
//                                         'font-size': '24'}})
//        .addTo(map);
        ";
        unset($state->polygon);
    }
    
    foreach ($cities as $city) {
        if (!$city->polygon) {
            continue;
        }
        
        $js .= "cities[{$city->id}] = L.polygon([".json_encode($city->polygon->data)."],{
            color: '#000',
            opacity: 1,
            fillColor: '#fff',
            fillOpacity: 0.5,
            weight: 1,
            title: '{$city->name}'
        })
        .bindTooltip('{$city->name}', {permanent: false, className: 'map-city-tooltip', offset: [0, 0], direction: 'center' })
        ;
        cities[{$city->id}].on('click', cityClick);
        cities[{$city->id}].data = {
            'id': {$city->id},
            'name': '{$city->name}',
        }
        cities[{$city->id}].addTo(map);";
        
        unset($city->polygon);
    }
    
    $this->registerJs($js);
    
