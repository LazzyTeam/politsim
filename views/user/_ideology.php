<?php

use app\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this \yii\web\View */
/* @var $ideologies app\models\variables\Ideology[] */
/* @var $user app\models\auth\User */

?>
<?php $form = ActiveForm::begin([
    'action' => ['/user/ideology'],
    'method' => 'POST',
]) ?>
<div class="form-group">
    <label for="#new-ideology-id"><?=Yii::t('app', 'New ideology')?></label>
    <?= Select2::widget([
        'name' => 'id',
        'value' => $user->ideologyId,
        'data' => ArrayHelper::map($ideologies, 'id', 'name'),
        'id' => 'new-ideology-id',
        'options' => [
            'class' => 'form-control',
        ],
        'pluginOptions' => [
            'placeholder' => 'Select ideology',
        ],
    ]) ?>
</div>
<div class="form-group">
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-flat']) ?>
</div>
<?php ActiveForm::end() ?>