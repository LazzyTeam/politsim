<?php

use app\helpers\Html,
    app\helpers\LinkCreator;

/* @var $this yii\base\View */
/* @var $region app\models\map\Region */
/* @var $user app\models\User */

$this->title = $region->name;

?>
<section class="content-header">
    <h1>
        <?=Html::encode($region->name)?>
    </h1>
    <ol class="breadcrumb">
        <?php if ($region->state): ?><li><?=LinkCreator::stateLink($region->state)?></li><?php endif ?>
        <li class="active"><?=$region->flag ? Html::img($region->flag, ['style' => 'height: 8px; vertical-align: baseline;']) : ''?> <?=Html::encode($region->name)?></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <?php if ($region->flag || $region->anthem): ?>
        <div class="col-md-4">
            <?php if ($region->flag): ?>
                <div class="box">
                    <div class="box-body">
                        <?=Html::img($region->flag, ['class' => 'img-polaroid', 'style' => 'width: 100%'])?>
                    </div>
                    <div class="box-footer">
                        <em><?=Yii::t('app', 'Region flag')?></em>
                    </div>
                </div>
            <?php endif ?>
            <?php if ($region->anthem): ?>
                <div class="box">
                    <div class="box-body">
                        <iframe id="sc-widget" src="https://w.soundcloud.com/player/?url=<?= $region->anthem ?>" width="100%" height="100" scrolling="no" frameborder="no"></iframe>
                    </div>
                    <div class="box-footer">
                        <em><?=Yii::t('app', 'Region anthem')?></em>
                    </div>
                </div>
            <?php endif ?>
        </div>
        <?php endif ?>
        <div class="col-md-<?=($region->flag || $region->anthem)?8:12?>">            
            <div class="box">
                <div class="box-header">
                    <h1>
                        <?=Html::encode($region->name)?>
                         <small>(<?=Html::encode($region->nameShort)?>)</small>
                    </h1>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <table class="table table-bordered no-margin">
                                <tbody>
                                    <tr>
                                        <td style="min-width: 120px"><strong><i class="fa fa-group"></i> <?=Yii::t('app', 'Population')?></strong></td>
                                        <td><?=Html::numberWord($region->population, 'h')?> <?=Html::a(Yii::t('app', 'Population info'),'/population/region?id='.$region->id,['class' => 'btn btn-info btn-xs'])?></td>
                                    </tr>
                                    <tr>
                                        <td style="min-width: 120px"><strong><i class="fa fa-map"></i> <?=Yii::t('app', 'Area')?></strong></td>
                                        <td><?= $region->area ?> кв.км.</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6 col-sm-12">
                        <?php if (count($region->cities)): ?>
                            <h4>Крупные города</h4>
                            <table class="table table-bordered no-margin">
                                <tbody>
                                    <?php foreach ($region->cities as $city): ?>
                                    <tr>
                                        <td><?= LinkCreator::cityLink($city) ?></td>
                                        <td><?= Html::numberWord($city->population, 'h') ?></td>
                                    </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        <?php endif ?>
                        <?php if (!$region->state): ?>
                            <p><?= Yii::t('app', 'This region have no control') ?></p>
                            <?= Html::a(Yii::t('app', 'Constituent assembly'), "/constituent-assembly/create?regionId={$region->id}", ['class' => 'btn btn-success']) ?>
                        <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>