<?php

use app\helpers\Html;

/* @var $this yii\web\View */
/* @var $models app\models\economy\resources\Resource[] */

$this->title = 'Ресурсы';
$this->params['breadcrumbs'][] = $this->title;

?>
<table class="table table-bordered table-hover">
    <tbody>
    <?php foreach($models as $model): ?>
        <tr>
            <td><?= $model->name ?></td>
            <td><?= Html::a("Редактировать", ["/admin/resource/update", "id" => $model->id], ['class' => 'btn btn-primary']) ?></td>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>