<?php

namespace app\models\base\traits;

use app\helpers\SoundCloudLink;

trait AnthemOwnerTrait
{

    /**
     * 
     * @return boolean
     */
    public function validateAnthem() {
        if (!SoundCloudLink::validate($this->anthem)) {
            $this->addError('anthem', Yii::t('app', 'Anthem are not valid SoundCloud link'));
            return false;
        }
        return true;
    }
    
}
