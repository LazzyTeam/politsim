<?php

namespace app\models\politics;

use Yii;
use app\models\base\ActiveRecord;
use app\models\auth\User;

/**
 * This is the model class for table "organizationsPosts".
 *
 * @property integer $id
 * @property integer $orgId
 * @property integer $userId
 * @property string $name
 * @property string $nameShort
 * @property integer $powers
 * @property integer $appointmentType
 * @property integer $successorId
 *
 * @property Organization $organization
 * @property User $user
 * @property User $successor
 */
class OrganizationPost extends ActiveRecord
{

    /**
     * изменение основных полей организации
     */
    const POWER_CHANGE_FIELDS = 1;
    
    /**
     * создание и редактирование постов
     */
    const POWER_EDIT_POSTS = 2;
    
    /**
     * принятие заявок в партию
     */
    const POWER_APPROVE_REQUESTS = 4;
    
    
    const APPOINTMENT_TYPE_UNKNOWN = 0;
    
    /**
     * назнач лидером
     */
    const APPOINTMENT_TYPE_LEADER = 1;
    
    /**
     * назначается предыдущим владельцем
     */
    const APPOINTMENT_TYPE_INHERITANCE = 2;
    
    /**
     * выбирается на праймериз
     */
    const APPOINTMENT_TYPE_PRIMARIES = 3;
    
    
    /**
     * Дефолтные данные для создания поста лидера новой организации
     */
    const DEFAULT_LEADER_DATA = [
        'name' => 'Лидер',
        'nameShort' => 'Л',
        'powers' => self::POWER_CHANGE_FIELDS & self::POWER_EDIT_POSTS & self::POWER_APPROVE_REQUESTS,
        'appointmentType' => self::APPOINTMENT_TYPE_INHERITANCE,
        'successorId' => null,
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organizationsPosts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['orgId', 'name', 'nameShort', 'powers', 'appointmentType'], 'required'],
            [['orgId', 'userId', 'powers', 'appointmentType', 'successorId'], 'default', 'value' => null],
            [['orgId', 'userId', 'powers', 'appointmentType', 'successorId'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['nameShort'], 'string', 'max' => 10],
            [['orgId'], 'exist', 'skipOnError' => true, 'targetClass' => Organization::class, 'targetAttribute' => ['orgId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['userId' => 'id']],
            [['successorId'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['successorId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'orgId' => 'Org ID',
            'userId' => 'User ID',
            'name' => 'Name',
            'nameShort' => 'Name Short',
            'powers' => 'Powers',
            'appointmentType' => 'Appointment Type',
            'successorId' => 'Successor ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganization()
    {
        return $this->hasOne(Organization::class, ['id' => 'orgId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuccessor()
    {
        return $this->hasOne(User::class, ['id' => 'successorId']);
    }

}
