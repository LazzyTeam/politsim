<?php

namespace app\models\map;

use Yii;
use app\models\base\ActiveRecord;
use app\models\base\interfaces\MapObject;
use app\models\base\taxpayers\TaxPayerInterface;
use app\models\economy\UtrType;
use app\models\government\State;

/**
 * This is the model class for table "regions".
 *
 * @property integer $id
 * @property integer $stateId
 * @property integer $cityId
 * @property string $name
 * @property string $nameShort
 * @property string $flag
 * @property string $anthem
 * @property integer $population
 * @property integer $area
 * @property integer $usersCount
 * @property integer $usersFame
 * @property integer $dateCreated
 * @property integer $dateDeleted
 * @property integer $implodedTo
 * @property integer $utr
 *
 * @property City[] $cities
 * @property City $capital
 * @property Region $implodedToObject
 * @property State $state
 * @property Tile[] $tiles
 * @property Polygon $polygon
 */
class Region extends ActiveRecord implements MapObject, TaxPayerInterface
{

    use \app\models\base\taxpayers\TaxPayerTrait;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'regions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stateId', 'cityId', 'dateCreated', 'dateDeleted', 'implodedTo', 'utr'], 'default', 'value' => null],
            [['population', 'area', 'usersCount', 'usersFame'], 'default', 'value' => 0],
            [['stateId', 'cityId', 'population', 'area', 'usersCount', 'usersFame', 'dateCreated', 'dateDeleted', 'implodedTo', 'utr'], 'integer'],
            [['name', 'nameShort'], 'required'],
            [['name', 'flag', 'anthem'], 'string', 'max' => 255],
            [['nameShort'], 'string', 'max' => 10],
            [['utr'], 'unique'],
            [['cityId'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['cityId' => 'id']],
            [['implodedTo'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['implodedTo' => 'id']],
//            [['stateId'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['stateId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'stateId' => 'Государство',
            'cityId' => 'Столица',
            'name' => 'Название',
            'nameShort' => 'Краткое название (код)',
            'flag' => 'Флаг',
            'anthem' => 'Гимн',
            'population' => 'Население',
            'area' => 'Площадь ',
            'usersCount' => 'Число игроков',
            'usersFame' => 'Суммарная известность игроков',
            'dateCreated' => 'Дата формирования',
            'dateDeleted' => 'Дата расформирования',
            'implodedTo' => 'Объединён с регионом',
            'utr' => 'ИНН',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::class, ['regionId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCapital()
    {
        return $this->hasOne(City::class, ['id' => 'cityId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImplodedToObject()
    {
        return $this->hasOne(Region::class, ['id' => 'implodedTo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::class, ['id' => 'stateId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTiles()
    {
        return $this->hasMany(Tile::class, ['regionId' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolygon()
    {
        return $this->hasOne(Polygon::class, ['ownerId' => 'id'])->andWhere(['ownerType' => Polygon::TYPE_REGION]);
    }

    public function getTaxStateId(): int
    {
        return $this->stateId;
    }

    public function getUserControllerId()
    {
        return null;
    }

    public function getUtrType(): int
    {
        return UtrType::REGION;
    }

    public function isGovernment(int $stateId): bool
    {
        return $this->stateId === $stateId;
    }

    public function isTaxedInState(int $stateId): bool
    {
        return $this->stateId === $stateId;
    }

    public function isUserController(int $userId): bool
    {
        return false;
    }

    public function getMapColor(): string
    {
        $colors = ['cc3399', '330066', '00ccff', '00ff66', 'cccc99', 'ff3366', 'cc0033', 'ff0033', 'ff9999', 'cc3366', 'ffccff', 'cc6699', '660033', 'ff99cc', 'ff66cc', 'ff99ff', 'ff6699', 'cc0066', 'ff3399', 'ff0099', 'ff33cc', 'ff00cc', 'ff66ff', 'ff33ff', 'ff00ff', '990066', 'cc66cc', 'cc33cc', 'cc99ff', 'cc66ff', 'cc33ff', '993399', 'cc00ff', '9900cc', '990099', 'cc99cc', '996699', '663366', '660099', '660066', '9900ff', '9933ff', '9966cc', '330033', '663399', '6633cc', '9966ff', '6600ff', '6633ff', 'ccccff', '9999ff', '9999cc', '6666ff', '666699', '333366', '333399', '330099', '3300cc', '3300ff', '3333cc', '0066ff', '0033ff', '3366ff', '3366cc', '000066', '000033', '000099', '0033cc', '0000cc', '336699', '0066cc', '99ccff', '6699ff', '6699cc', '006699', '3399cc', '0099cc', '66ccff', '3399ff', '003399', '33ccff', '99ffff', '66ffff', '33ffff', '00ffff', '00cccc', '669999', '99cccc', 'ccffff', '33cccc', '66cccc', '339999', '336666', '003333', '00ffcc', '33ffcc', '33cc99', '00cc99', '66ffcc', '99ffcc', '339966', '006633', '336633', '669966', '66cc66', '99ff99', '66ff66', '99cc99', '66ff99', '33ff99', '33cc66', '00cc66', '66cc99', '009966', '33ff66', 'ccffcc', 'ccff99', '99ff66', '99ff33', '00ff33', '00cc33', '33cc33', '66ff33', '00ff00', '66cc33', '006600', '003300', '33ff00', '66ff00', '99ff00', '66cc00', '00cc00', '33cc00', '339900', '669933', '99cc33', '336600', '669900', '99cc00', 'ccff66', 'ccff33', '999900', 'cccc00', 'cccc33', '333300', '666600', '999933', 'cccc66', '999966', 'ffffcc', 'ffff99', 'ffff66', 'ffff33', 'ffff00', 'ffcc66', 'ffcc33', 'cc9933', '996600', 'cc9900', 'ff9900', 'cc6600', 'cc6633', '663300', 'ff9966', 'ff6633', 'ff9933', 'ff6600', 'cc3300', '330000', '663333', '996666', 'cc9999', '993333', 'cc6666', 'ffcccc', 'cc3333', 'ff6666', '660000', '990000', 'cc0000', 'ff0000', 'ff3300', 'ffcc99', 'cccccc', '999999', '666666', '333333'];
        return $colors[$this->id % count($colors)];
    }

    public function getTooltipName(): string
    {
        return $this->name;
    }

}
