<?php

namespace app\models\government;

use Yii;
use app\models\map\Region;
use app\models\auth\User;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "constituentAssemblies".
 *
 * @property int $id
 * @property int $regionId
 * @property int $creatorId
 * @property array $data
 * @property int $stateId
 * @property int $dateCreated
 * @property int $dateFinished
 *
 * @property Region $region
 * @property State $state
 * @property User $creator
 * @property ConstituentAssemblyMembership[] $memberships
 * @property User[] $users
 * @property ConstituentAssemblyData[] $dataObjects
 */
class ConstituentAssembly extends \app\models\base\ActiveRecord
{
    
    public $dataObjects = [];
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'constituentAssemblies';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
	return [
	    [
		'class' => TimestampBehavior::className(),
		'createdAtAttribute' => 'dateCreated',
		'updatedAtAttribute' => false,
	    ],
	];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['regionId', 'creatorId'], 'required'],
            [['regionId', 'creatorId', 'stateId', 'dateCreated', 'dateFinished'], 'default', 'value' => null],
            [['regionId', 'creatorId', 'stateId', 'dateCreated', 'dateFinished'], 'integer'],
            [['data'], 'safe'],
            [['data'], 'default', 'value' => []],
            [['regionId'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['regionId' => 'id']],
            [['stateId'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['stateId' => 'id']],
            [['creatorId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creatorId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'regionId' => Yii::t('app', 'Region ID'),
            'creatorId' => Yii::t('app', 'Creator ID'),
            'data' => Yii::t('app', 'Data'),
            'stateId' => Yii::t('app', 'State ID'),
            'dateCreated' => Yii::t('app', 'Date Created'),
            'dateFinished' => Yii::t('app', 'Date Finished'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'regionId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'stateId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creatorId']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMemberships()
    {
        return $this->hasMany(ConstituentAssemblyMembership::className(), ['assemblyId' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'userId'])->viaTable('constituentAssembliesMemberships', ['assemblyId' => 'id']);
    }
    
    public function afterFind()
    {
        foreach ($this->data as $userId => $userData) {
            $this->dataObjects[$userId] = new ConstituentAssemblyData($userData);
        }
        return parent::afterFind();
    }
    
    public function beforeSave($insert)
    {
        $data = [];
        foreach ($this->dataObjects as $userId => $userData) {
            $data[$userId] = $userData->toArray();
        }
        $this->data = $data;
        return parent::beforeSave($insert);
    }
    
    public function getVariants($article)
    {
        $variants = [];
        foreach ($this->dataObjects as $userId => $userData) {
            $var = $userData->{$article};
            if ($var) {
                $variants[$userId] = $var;
            }
        }
        return $variants;
    }
}
