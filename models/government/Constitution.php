<?php

namespace app\models\government;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "constitutions".
 *
 * @property int $id
 * @property int $stateId
 * @property array $data
 * @property int $dateCreated
 * @property int $dateConfirmed
 * 
 * @property boolean $isConfirmed
 *
 * @property State $state
 */
class Constitution extends \app\models\base\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'constitutions';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
	return [
	    [
		'class' => TimestampBehavior::className(),
		'createdAtAttribute' => 'dateCreated',
		'updatedAtAttribute' => false,
	    ],
	];
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['stateId', 'dateConfirmed'], 'default', 'value' => null],
            [['stateId', 'dateCreated', 'dateConfirmed'], 'integer'],
            [['data'], 'required'],
            [['data'], 'safe'],
            [['stateId'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['stateId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'stateId' => Yii::t('app', 'State ID'),
            'data' => Yii::t('app', 'Data'),
            'dateCreated' => Yii::t('app', 'Date Created'),
            'dateConfirmed' => Yii::t('app', 'Date Confirmed'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::class, ['id' => 'stateId']);
    }
    
    public function getIsConfirmed()
    {
        return !!$this->dateConfirmed;
    }
    
    public function confirm($save = true)
    {
        $this->dateConfirmed = time();
        if ($save) {
            return $this->save();
        }
    }
    
}
