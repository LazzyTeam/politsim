<?php

namespace app\models\economy\resources\types;

use Yii;
use app\models\economy\resources\Resource;

/**
 * Нефть хуефть
 *
 * @author ilya
 */
class Gas extends Resource
{
    
    public function getName(): string
    {
        return 'Газ';
    }

}
